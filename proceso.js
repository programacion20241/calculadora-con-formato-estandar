let visor = document.querySelector(".visor");
// COMO NECESITAMOS UNA COLECCION DE BOTONES, ENTONCES USAREMOS GetElementByClassName
let botones = document.getElementsByClassName("boton");
console.log("Colección de botones",botones);

for (let i =  0; i < botones.length; i++) {
    console.log("Agregamos el listener a: ", botones[i]);
    botones[i].addEventListener("click", () => {
        accion(botones[i]);
    });
}

function accion(boton) {
    console.log("Valor del botón presionado", boton.innerHTML);
    switch (boton.innerHTML) {
        case 'C':
            console.log("Con C borra el visor");
            borrar(); 
            break;
            case '=':
            console.log("Con =, calcula el resultado");
            resultado(); 
            break;
        default:
            console.log("Va armado el visor");
            actualizarVisor(boton);
            break;
    }
    
}

function borrar(){
    console.log("Procedemos al borrado del visor");
    visor.innerHTML = "0";
}

function actualizarVisor(boton) {
    if (visor.innerHTML == 0) {
        visor.innerHTML = "";
    }
    visor.innerHTML += boton.innerHTML;
}
function resultado() {
    console.log("intenta eval(uar) el contenido del visor");
    visor.innerHTML = eval(visor.innerHTML); //INTENTA EVALUAR Y EJECUTAR LO QUE ESTÁ ESCRITO EN LO QUE LEE (visor.innerHTML)
    
}